Prepare.
--------

Install cross-toolchain for ARM.
```
$ sudo apt-get install gcc-arm-linux-gnueabi
```

Install QEMU for ARM architecture emulation.
```
$ sudo apt-get install qemu-system-arm
```

Test QEMU with ARM kernel and userspace.
```
$ qemu-system-arm \
        -machine virt \
        -kernel /path/to/zImage \
        -initrd /path/to/rootfs.cpio \
        -nographic \
        -m 512 \
        --append " \
            root=/dev/ram0 \
            rw \
            console=ttyAMA0,38400 \
            console=ttyS0 \
            mem=512M \
            loglevel=9"
```

Download Linux Kernel sources.
```
$ git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git linux.org
$ cd linux.org
$ git checkout -b v4.8 v4.8
```

Create woring copy.
```
$ cp -a linux.org linux.new
```

Try to build Linux Kernel.
```
$ cd linux.new
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- multi_v7_defconfig
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- zImage -j1
$ cp linux.new/arch/arm/boot/zImage .
```

Unpack ```initrd``` and prepare it for modifictations.
```
# mkdir rootfs
# cd rootfs
# cpio -idv < ../rootfs.cpio

```
Data layout after preparation.

```
$ tree -L 1 .
.
├── linux.new
├── linux.org
├── patch_dummy_hello_0x.diff
├── rootfs
├── rootfs.cpio
└── zImage
```

Write own driver.
-----------------

Create file.
```
$ cd linux.new
$ touch drivers/misc/dummy_hello.c
```

Modify configs.
```
$ cat drivers/misc/Kconfig
...
config DUMMY_HELLO                                                                                                                                                                          
       tristate "Dummy 'Hello' module"                                                                                                                                                      
       depends on INPUT                                                                                                                                                                     
       default n                                                                                                                                                                            
       help                                                                                                                                                                                 
         Here is dummy driver for educational purpose.                                                                                                                                      
...
```

Modify Makefile.
```
$ cat drivers/misc/Makefile
...
obj-$(CONFIG_DUMMY_HELLO)      += dummy_hello.o
...
```

Reconfigure source tree to build own driver.
```
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- multi_v7_defconfig
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- menuconfig
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- drivers/misc/dummy_hello.ko
```

Add module into ramfs
```
# cd rootfs
# cp ../linux.new/drivers/misc/dummy_hello.ko lib/dummy_hello.ko
# find . | cpio -o -H newc > ../rootfs.cpio
```

Run image into emulator and test it.


Create patch.
```
$ pwd
$ linux.new
$ cd ..
$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- mrproper
$ diff -uNr linux.org linux.new > patch_dummy_hello_0x.diff
```

Don't forget check code style.
```
$ cd linux.new
$ ./scripts/checkpatch.pl ../patch_dummy_hello_0x.diff
```

Apply (test) patch.
```
$ cd linux.org
$ patch -p1 < ../patch_dummy_hello_0x.diff
```

Workflow and description of patches.
------------------------------------

01 - Create module skeleton.
```
# insmod /lib/dummy_hello.ko 
[   58.267750] Hello inited.
# rmmod /lib/dummy_hello.ko 
[   60.648396] Hello exited.
# dmesg 
[   58.267750] Hello inited.
[   60.648396] Hello exited.
```

02 - Playing with return code from init function (return positive ERRNO). The kernel complaints on unconventional code but still loads the module.
```
# insmod /lib/dummy_hello.ko 
[   16.887798] Hello inited.
[   16.887955] do_init_module: 'dummy_hello'->init suspiciously returned 16, it should follow 0/-E convention
[   16.887955] do_init_module: loading module anyway...
[   16.888347] CPU: 0 PID: 108 Comm: insmod Not tainted 4.8.0-dirty #1
[   16.888445] Hardware name: Generic DT based system
[   16.888920] [<c0310480>] (unwind_backtrace) from [<c030ba04>] (show_stack+0x10/0x14)
[   16.889068] [<c030ba04>] (show_stack) from [<c0598188>] (dump_stack+0x88/0x9c)
[   16.889191] [<c0598188>] (dump_stack) from [<c03ce574>] (do_init_module+0x8c/0x1c8)
[   16.889311] [<c03ce574>] (do_init_module) from [<c03b1dc8>] (load_module+0x1c80/0x1fc4)
[   16.889453] [<c03b1dc8>] (load_module) from [<c03b2258>] (SyS_init_module+0x14c/0x15c)
[   16.889578] [<c03b2258>] (SyS_init_module) from [<c0307d00>] (ret_fast_syscall+0x0/0x3c)
# lsmod
Module                  Size  Used by    Not tainted
dummy_hello              734  0 
# rmmod /lib/dummy_hello.ko 
[   38.038280] Hello exited
```

03 - Playing with return code from init function (return negative ERRNO). The kernel doesn't load the module.
```
# insmod /lib/dummy_hello.ko 
[   32.882550] Hello inited.
insmod: can't insert '/lib/dummy_hello.ko': Device or resource busy
# lsmod 
Module                  Size  Used by    Not tainted
#
```

04 - Trying dereference a null pointer inside init function. Segmentation fault is caused but the module is still loaded.
```
# insmod /lib/dummy_hello.ko 
[   14.666995] Unhandled fault: page domain fault (0x81b) at 0x00000000
[   14.667202] pgd = d1e64000
[   14.667319] [00000000] *pgd=51e05835, *pte=00000000, *ppte=00000000
[   14.667978] Internal error: : 81b [#1] SMP ARM
[   14.668150] Modules linked in: dummy_hello(+)
[   14.668499] CPU: 0 PID: 108 Comm: insmod Not tainted 4.8.0-dirty #1
[   14.668619] Hardware name: Generic DT based system
[   14.668774] task: d1e25a40 task.stack: d1e74000
[   14.669123] PC is at do_gettimeofday+0x34/0x5c
[   14.669277] LR is at 0x8000000
[   14.669359] pc : [<c039fd40>]    lr : [<08000000>]    psr: 600b0013
[   14.669359] sp : d1e75de8  ip : c120255c  fp : 00000024
[   14.669561] r10: bf0000c0  r9 : 2e1dcddc  r8 : 00000000
[   14.669654] r7 : d1e23240  r6 : d1e23300  r5 : bf002000  r4 : 00000000
[   14.669788] r3 : 00000000  r2 : 58eca0d8  r1 : 015065d3  r0 : f8f53230
[   14.669950] Flags: nZCv  IRQs on  FIQs on  Mode SVC_32  ISA ARM  Segment none
[   14.670080] Control: 10c5387d  Table: 51e6406a  DAC: 00000051
[   14.670204] Process insmod (pid: 108, stack limit = 0xd1e74220)
[   14.670319] Stack: (0xd1e75de8 to 0xd1e76000)
[   14.670520] 5de0:                   58eca0d8 00000000 14883710 d1e0c300 00000000 bf00201c
[   14.670742] 5e00: ffffe000 c0301e9c c0f01310 8040003f c0e67da0 bf000108 00000000 c123eac4
[   14.670958] 5e20: c123eadc 8040003e dbe15460 00004d87 1aab2000 dbe15460 00004d87 1aab2000
[   14.671206] 5e40: dbe15460 00004d87 d1e23240 00000001 bf0000c0 d1e75f44 bf0000c0 d1e75f44
[   14.671448] 5e60: d1e23300 d1e23240 00000001 c03ce548 d1e75f44 d1e23264 00000001 d1e75f44
[   14.671679] 5e80: d1e23264 c03b1dc8 bf0000cc 00007fff bf0000c0 c03af4d0 bf0000c0 bf000108
[   14.671919] 5ea0: 00000000 bf00027c d1e75edc e0b3fdd0 bf0000cc 00000000 c0c07fbc c0e6c044
[   14.672161] 5ec0: c0e6bfe0 c0e6bfec d1e0ca40 c0403d2c 00400000 024002c2 d1e0ca40 00000000
[   14.672393] 5ee0: 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
[   14.672632] 5f00: 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000e20
[   14.672866] 5f20: 00000000 000c0e38 e0b3fe20 d1e74000 000c0008 00000051 00000000 c03b2258
[   14.673115] 5f40: da829428 e0b3f000 00000e20 e0b3fa38 e0b3f924 e0b3f758 000002c4 00000304
[   14.673370] 5f60: 00000000 00000000 00000000 00000454 00000017 00000018 00000010 00000000
[   14.673622] 5f80: 0000000d 00000000 00000e20 befdaf44 00000002 00000080 c0307ec4 d1e74000
[   14.673880] 5fa0: 00000000 c0307d00 00000e20 befdaf44 000c0018 00000e20 000c0008 befdaf44
[   14.674122] 5fc0: 00000e20 befdaf44 00000002 00000080 00000000 00000000 b6f66000 00000000
[   14.674365] 5fe0: befdaca0 befdac90 000240c8 b6e9c8b0 600b0010 000c0018 00000000 00000000
[   14.674836] [<c039fd40>] (do_gettimeofday) from [<bf00201c>] (hello_init+0x1c/0x30 [dummy_hello])
[   14.675140] [<bf00201c>] (hello_init [dummy_hello]) from [<c0301e9c>] (do_one_initcall+0x44/0x170)
[   14.675341] [<c0301e9c>] (do_one_initcall) from [<c03ce548>] (do_init_module+0x60/0x1c8)
[   14.675524] [<c03ce548>] (do_init_module) from [<c03b1dc8>] (load_module+0x1c80/0x1fc4)
[   14.675721] [<c03b1dc8>] (load_module) from [<c03b2258>] (SyS_init_module+0x14c/0x15c)
[   14.675899] [<c03b2258>] (SyS_init_module) from [<c0307d00>] (ret_fast_syscall+0x0/0x3c)
[   14.676159] Code: e3410062 e59d2000 e0c10093 e1a03fc3 (e5842000) 
[   14.676457] ---[ end trace b3d89ed773429a0d ]---
Segmentation fault
# lsmod
Module                  Size  Used by    Tainted: G  
dummy_hello             1922  1 
# rmmod /lib/dummy_hello.ko 
rmmod: can't unload module 'dummy_hello': Device or resource busy
# rmmod -f /lib/dummy_hello.ko 
rmmod: can't unload module 'dummy_hello': Device or resource busy
#
```

05 - Trying use memory after it's freed inside exit function. Occurs a logical error but without memory corruptions.
```
# insmod /lib/dummy_hello.ko 
[   35.414298] Hello inited.
# rmmod /lib/dummy_hello.ko 
[   43.747974] Hello exited after about -2029445111 seconds.
# dmesg 
[   35.414298] Hello inited.
[   43.747974] Hello exited after about -2029445111 seconds.
#
```

06 - Final version which looks good.
```
# insmod /lib/dummy_hello.ko 
[   25.133966] Hello inited.
# rmmod /lib/dummy_hello.ko 
[   35.048613] Hello exited after about 10 seconds.
# dmesg
[   25.133966] Hello inited.
[   35.048613] Hello exited after about 10 seconds.
#
```

07 - Cleanup patch.

08 - Hard interrupt handling (with predefined IRQ number).
```
# insmod /lib/dummy_hello.ko
[   34.035385] Hello inited.
# [   37.553570] Something.l
[   37.818346] Something.sSomething.m
[   38.201922] Something.oSomething.d
[   38.554841] Something.
Module                  Size  Used by    Not tainted
dummy_hello              919  0 
# 
```

09 - Hard interrupt handling with sleeping in the handler (with predefined IRQ number). Sleeping in the handler leads to endless errors.
```
# insmod /lib/dummy_hello.ko
[   34.035385] Hello inited.
...
[   16.064103] bad: scheduling from the idle thread!
[   16.064184] CPU: 0 PID: 0 Comm: swapper/0 Tainted: G        W       4.8.0-dirty #3
[   16.064290] Hardware name: Generic DT based system
[   16.064375] [<c0310480>] (unwind_backtrace) from [<c030ba04>] (show_stack+0x10/0x14)
[   16.064499] [<c030ba04>] (show_stack) from [<c0598188>] (dump_stack+0x88/0x9c)
[   16.064625] [<c0598188>] (dump_stack) from [<c0369184>] (dequeue_task_idle+0x34/0x40)
[   16.064755] [<c0369184>] (dequeue_task_idle) from [<c0ba88d0>] (__schedule+0x214/0x564)
[   16.064887] [<c0ba88d0>] (__schedule) from [<c0ba8c64>] (schedule+0x44/0x9c)
[   16.065007] [<c0ba8c64>] (schedule) from [<c0bab998>] (schedule_timeout+0x150/0x234)
[   16.065141] [<c0bab998>] (schedule_timeout) from [<c039797c>] (msleep+0x2c/0x38)
[   16.065269] [<c039797c>] (msleep) from [<bf00000c>] (hello_irq_handler+0xc/0x20 [dummy_hello])
[   16.065399] [<bf00000c>] (hello_irq_handler [dummy_hello]) from [<c0387954>] (__handle_irq_event_percpu+0x9c/0x124)
[   16.065552] [<c0387954>] (__handle_irq_event_percpu) from [<c03879f8>] (handle_irq_event_percpu+0x1c/0x58)
[   16.065698] [<c03879f8>] (handle_irq_event_percpu) from [<c0387a78>] (handle_irq_event+0x44/0x68)
[   16.065829] [<c0387a78>] (handle_irq_event) from [<c038aec4>] (handle_fasteoi_irq+0xb4/0x194)
[   16.065960] [<c038aec4>] (handle_fasteoi_irq) from [<c0386f8c>] (generic_handle_irq+0x24/0x34)
[   16.066098] [<c0386f8c>] (generic_handle_irq) from [<c038725c>] (__handle_domain_irq+0x5c/0xb4)
[   16.066227] [<c038725c>] (__handle_domain_irq) from [<c0301778>] (gic_handle_irq+0x54/0x98)
[   16.066356] [<c0301778>] (gic_handle_irq) from [<c030c58c>] (__irq_svc+0x6c/0x90)
[   16.066468] Exception stack(0xc1201f50 to 0xc1201f98)
[   16.066591] 1f40:                                     00000001 00000000 00000000 c031b340
[   16.066793] 1f60: c1200000 c12024a4 00000001 00000000 00000000 c1116040 c1202510 c1202508
[   16.066974] 1f80: 600f0013 c1201fa0 c03087dc c03087e0 600f0013 ffffffff
[   16.067099] [<c030c58c>] (__irq_svc) from [<c03087e0>] (arch_cpu_idle+0x38/0x3c)
[   16.067223] [<c03087e0>] (arch_cpu_idle) from [<c037ab64>] (cpu_startup_entry+0x1cc/0x228)
[   16.067357] [<c037ab64>] (cpu_startup_entry) from [<c1000c58>] (start_kernel+0x36c/0x378)
...
```

10 - Switch to threaded version (with predefined IRQ number) and print current PID. Now we can sleep in bottom half of the handler.
```
# dmesg 
[  102.218289] Hello inited.
[  103.555138] Hello from: 0.
[  104.089615] Hello from: 118.
[  105.288740] Hello from: 0.
[  105.810059] Hello from: 118.
[  106.784779] Hello from: 0.
[  107.329756] Hello from: 118.
[  107.330100] Hello exited.
# 
```

11 - Cleanup patch.

12 - Module with support communication through debugfs (simple version).
```
# mount -t debugfs none /sys/kernel/debug/
# insmod /lib/dummy_hello.ko 
[   81.090765] Hello inited.
# cat /sys/kernel/debug/dummy-hello/uart-pl011-count 
20
# cat /sys/kernel/debug/dummy-hello/uart-pl011-count 
22
# rmmod /lib/dummy_hello.ko 
[  120.588751] Hello exited.
# dmesg 
[   81.090765] Hello inited.
[  120.588751] Hello exited.
#
```

13 - Module with support communication through debugfs. Patch contains trivial memory allocator.
   The allocator use only one page and provide memory for internal usage. More details you find
   in comments. Patch also contains explicit defined file operations. Actually for writing only
   one function is used for both files.
```
# insmod /lib/dummy_hello.ko 
[   20.843649] Hello allocator: requested 48, free memory offset 48.
[   20.843857] Hello allocator: requested 33, free memory offset 96.
[   20.843996] Hello allocator: requested 192, free memory offset 288.
[   20.844126] Hello allocator: requested 33, free memory offset 336.
[   20.845425] Hello inited.
# mount -t debugfs non /sys/kernel/debug/
# cat /sys/kernel/debug/dummy-hello/uart-pl011-count 
62
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
73 32-49-097
# date
Sun Apr 16 06:32:49 UTC 2017
# echo 900 > /sys/kernel/debug/dummy-hello/uart-pl011-count 
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
904 33-22-281
# echo 0 > /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
# cat /sys/kernel/debug/dummy-hello/uart-pl011-count 
7
# rmmod /lib/dummy_hello.ko 
[  145.616083] Hello exited.
# dmesg 
[   20.843649] Hello allocator: requested 48, free memory offset 48.
[   20.843857] Hello allocator: requested 33, free memory offset 96.
[   20.843996] Hello allocator: requested 192, free memory offset 288.
[   20.844126] Hello allocator: requested 33, free memory offset 336.
[   20.845425] Hello inited.
[  145.616083] Hello exited.
```

14 - For features implemented in previous patch added ability to toggle timestamp processing mode.
```
# insmod /lib/dummy_hello.ko 
[   34.723278] Hello allocator: requested 52, free memory offset 64.
[   34.723586] Hello allocator: requested 33, free memory offset 112.
[   34.723757] Hello allocator: requested 192, free memory offset 304.
[   34.723925] Hello allocator: requested 33, free memory offset 352.
[   34.724375] Hello inited.
# mount -t debugfs none /sys/kernel/debug/
# cat /sys/kernel/debug/dummy-hello/uart-pl011-count 
56
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
0 00-00-000
# echo start > /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
[   63.231926] Hello: start processing timestamp.
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
90 24-48-272
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
92 24-49-903
# echo stop > /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
[   85.148089] Hello: stop processing timestamp.
# echo clear > /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
[   94.376210] Hello: clear timestamp buffer.
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
0 00-00-000
# echo 0 > /sys/kernel/debug/dummy-hello/uart-pl011-count 
# echo start > /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
[  134.216337] Hello: start processing timestamp.
# cat /sys/kernel/debug/dummy-hello/uart-pl011-timestamp 
37 25-59-149
# rmmod /lib/dummy_hello.ko 
[  152.175721] Hello exited.
# dmesg 
[   34.723278] Hello allocator: requested 52, free memory offset 64.
[   34.723586] Hello allocator: requested 33, free memory offset 112.
[   34.723757] Hello allocator: requested 192, free memory offset 304.
[   34.723925] Hello allocator: requested 33, free memory offset 352.
[   34.724375] Hello inited.
[   63.231926] Hello: start processing timestamp.
[   85.148089] Hello: stop processing timestamp.
[   94.376210] Hello: clear timestamp buffer.
[  134.216337] Hello: start processing timestamp.
[  146.932866] random: fast init done
[  152.175721] Hello exited.
```
